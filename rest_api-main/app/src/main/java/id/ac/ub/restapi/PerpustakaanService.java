package id.ac.ub.restapi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PerpustakaanService {
    @GET("buku")
    Call<List<Buku>> listBuku();

    @GET("buku/{id}")
    Call<Buku> getBuku(@Path("id") String id);

    @POST("users")
        //on below line we are creating a method to post our data.
    Call<DataInput> createPost(@Body DataInput dataInput);

}

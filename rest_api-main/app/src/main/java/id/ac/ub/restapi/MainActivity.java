//M Ighfar Maulana
//205150400111055

package id.ac.ub.restapi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.ac.ub.restapi.databinding.ActivityMainBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    ArrayList<Buku> listBuku = new ArrayList<>();
    EditText etJudul, etDeskripsi;
    Button btPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        BukuAdapter adapter=new BukuAdapter(this,listBuku);
        binding.recyclerView.setAdapter(adapter);
        PerpustakaanService perpustakaanService = RetrofitClient.getClient().create(PerpustakaanService.class);
        Call<List<Buku>> listRequest = perpustakaanService.listBuku();
        listRequest.enqueue(new Callback<List<Buku>>() {
            @Override
            public void onResponse(Call<List<Buku>> call, Response<List<Buku>> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
                    List<Buku> list = response.body();
                    Log.d("success", "list " + list.size());
                    listBuku.addAll(list);
                    adapter.notifyDataSetChanged();
                } else {
                    Log.d("errt", "" + response.errorBody());
                    Toast.makeText(MainActivity.this, "" + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Buku>> call, Throwable t) {
                Log.d("DataModel", "" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Error : " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        etJudul = findViewById(R.id.etJudul);
        etDeskripsi = findViewById(R.id.etDeskripsi);
        btPost = findViewById(R.id.btPost);

        btPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etJudul.getText().toString().isEmpty() || etDeskripsi.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Please enter both the values", Toast.LENGTH_SHORT).show();
                    return;
                }
                postData(etJudul.getText().toString(), etDeskripsi.getText().toString());
            }
        });
    }

    private void postData(String judul, String deskripsi) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.100.55/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PerpustakaanService perpustakaanService = retrofit.create(PerpustakaanService.class);

        DataInput modal = new DataInput(judul, deskripsi);

        Call<DataInput> call = perpustakaanService.createPost(modal);

        call.enqueue(new Callback<DataInput>() {
            @Override
            public void onResponse(Call<DataInput> call, Response<DataInput> response) {
                Toast.makeText(MainActivity.this, "Data added to API", Toast.LENGTH_SHORT).show();

                etJudul.setText("");
                etDeskripsi.setText("");

                // we are getting response from our body
                // and passing it to our modal class.
                //DataInput responseFromAPI = response.body();


            }

            @Override
            public void onFailure(Call<DataInput> call, Throwable t) {
                Log.d("DataModel", "" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Error : " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

package id.ac.ub.restapi;

public class DataInput {
    // string variables for our name and job
    private String judul;
    private String deskripsi;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public DataInput(String judul, String deskripsi) {
        this.judul = judul;
        this.deskripsi = deskripsi;
    }
}
